#include "drawObject.h"


drawObject::drawObject(GLfloat transX, GLfloat transZ, glm::vec3 scaleNums, GLuint cubeMesh, GLuint cubeIndex)
{
	translate.x = transX;
	translate.z = transZ;
	translate.y = scaleNums.y/2;  // sets the y position to + half the scale value so the object is drawn on ground level
	scale = scaleNums;
	mesh = cubeMesh;
	indexCount = cubeIndex;

	// sets the values for the box collision
	topCollision = transZ - scale.z/2;
	bottomCollision = transZ + scale.z/2;
	rightCollision = transX + scale.x/2;
	leftCollision = transX - scale.x/2;
}


drawObject::~drawObject(void)
{
}

void drawObject::draw(const GLuint shader, glm::mat4 modelview)
{
	modelview = glm::translate(modelview, translate);
	modelview = glm::scale(modelview, scale);
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawMesh(mesh,indexCount,GL_TRIANGLES);
}

void drawObject::draw(const GLuint shader, glm::mat4 modelview, GLfloat Ypos, glm::vec3 rotateNums, GLfloat degrees) // draw for independent y placement; used for the coins
{
	modelview = glm::translate(modelview, glm::vec3(translate.x, Ypos, translate.z));
	modelview = glm::scale(modelview, scale);
	modelview = glm::rotate(modelview, degrees, rotateNums);
	modelview = glm::rotate(modelview, -90.0f, glm::vec3(1.0f,0.0f,0.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawMesh(mesh,indexCount,GL_TRIANGLES);
}

bool drawObject::collision(glm::vec3 characterPos) // box collision
{
	GLfloat collisionBox = 1.2; // allows easy change to the dimensions of the collision box surrounding the character
	if ((characterPos.z +collisionBox < bottomCollision && characterPos.z +collisionBox > topCollision) && (characterPos.x -collisionBox < rightCollision && characterPos.x -collisionBox > leftCollision) || (characterPos.z +collisionBox < bottomCollision && characterPos.z +collisionBox > topCollision) && (characterPos.x +collisionBox < rightCollision && characterPos.x +collisionBox > leftCollision) ||
		(characterPos.z -collisionBox < bottomCollision && characterPos.z -collisionBox > topCollision) && (characterPos.x +collisionBox < rightCollision && characterPos.x +collisionBox > leftCollision) || (characterPos.z -collisionBox < bottomCollision && characterPos.z -collisionBox > topCollision) && (characterPos.x -collisionBox < rightCollision && characterPos.x -collisionBox > leftCollision))
		return true;

	return false;
}

bool drawObject::circleCollision(glm::vec3 characterPos)
{
	GLfloat distance = (characterPos.x - translate.x)*(characterPos.x - translate.x) + (characterPos.z - translate.z)*(characterPos.z - translate.z);
	if (distance <= 8.0f)
		return true;

	return false;
}

void drawObject::draw(const GLuint shader, glm::mat4 modelview, glm::vec3 rotateNums, GLfloat degrees) // draw with rotate
{
	modelview = glm::translate(modelview, translate);
	modelview = glm::scale(modelview, scale);
	modelview = glm::rotate(modelview, degrees, rotateNums);
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawMesh(mesh,indexCount,GL_TRIANGLES);
}
