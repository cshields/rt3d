#ifndef RT3D
#define RT3D

#include <GL/glew.h>
#include <SDL.h>
#include <iostream>
#include <fstream>
#include <string>
#include "bass.h"

#define RT3D_VERTEX		0
#define RT3D_COLOUR		1
#define RT3D_NORMAL		2
#define RT3D_TEXCOORD   3
#define RT3D_INDEX		4

namespace rt3d {

	struct lightStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat position[4];
	};

	struct materialStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat shininess;
	};

	void exitFatalError( char *message);
	char* loadFile(const char *fname, GLint &fSize);
	void printShaderError(const GLint shader);
	GLuint initShaders( char *vertFile,  char *fragFile);
	// Some methods for creating meshes
	// ... including one for dealing with indexed meshes
	GLuint createMesh( GLuint numVerts,  GLfloat* vertices,  GLfloat* colours,  GLfloat* normals,
		 GLfloat* texcoords,  GLuint indexCount,  GLuint* indices);
	// these three create mesh functions simply provide more basic access to the full version
	GLuint createMesh( GLuint numVerts,  GLfloat* vertices,  GLfloat* colours,  GLfloat* normals,
		 GLfloat* texcoords);
	GLuint createMesh( GLuint numVerts,  GLfloat* vertices);
	GLuint createColourMesh( GLuint numVerts,  GLfloat* vertices,  GLfloat* colours);

	void setUniformMatrix4fv(const GLuint program, const char* uniformName, const GLfloat *data);
	void setMatrices(const GLuint program, const GLfloat *proj, const GLfloat *mv, const GLfloat *mvp);
	void setLight(const GLuint program, const lightStruct light);
	void setLightPos( GLuint program,  GLfloat *lightPos);
	void setMaterial(const GLuint program, const materialStruct material);

	void drawMesh(const GLuint mesh, const GLuint numVerts, const GLuint primitive); 
	void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive);

	void updateMesh(const GLuint mesh, const unsigned int bufferType, const GLfloat *data, const GLuint size);
	HSAMPLE loadSample(char * filename);
	void playSound(HSAMPLE audio);
}

#endif