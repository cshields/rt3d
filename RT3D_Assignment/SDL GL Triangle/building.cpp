#include "building.h"


building::building(GLfloat transX, GLfloat transZ, glm::vec3 scaleNums, GLuint cubeMesh, GLuint cubeIndex)
{
	translate.x = transX;
	translate.z = transZ;
	translate.y = scaleNums.y/2;
	scale = scaleNums;
	mesh = cubeMesh;
	indexCount = cubeIndex;
	topCollision = transZ - scale.z/2;
	bottomCollision = transZ + scale.z/2;
	rightCollision = transX + scale.x/2;
	leftCollision = transX - scale.x/2;
}


building::~building(void)
{
}

void building::draw(const GLuint shader, glm::mat4 modelview)
{
	modelview = glm::translate(modelview, translate);
	modelview = glm::scale(modelview, scale);
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawMesh(mesh,indexCount,GL_TRIANGLES);
}

bool building::collision(glm::vec3 characterPos)
{
	GLfloat collisionBox = 1.2; // allows easy change to the dimensions of the collision box surrounding the character
	if ((characterPos.z +collisionBox < bottomCollision && characterPos.z +collisionBox > topCollision) && (characterPos.x -collisionBox < rightCollision && characterPos.x -collisionBox > leftCollision) || (characterPos.z +collisionBox < bottomCollision && characterPos.z +collisionBox > topCollision) && (characterPos.x +collisionBox < rightCollision && characterPos.x +collisionBox > leftCollision) ||
		(characterPos.z -collisionBox < bottomCollision && characterPos.z -collisionBox > topCollision) && (characterPos.x +collisionBox < rightCollision && characterPos.x +collisionBox > leftCollision) || (characterPos.z -collisionBox < bottomCollision && characterPos.z -collisionBox > topCollision) && (characterPos.x -collisionBox < rightCollision && characterPos.x -collisionBox > leftCollision))
		return true;

	return false;
}

void building::draw(const GLuint shader, glm::mat4 modelview, glm::vec3 rotateNums, GLfloat degrees)
{
	modelview = glm::translate(modelview, translate);
	modelview = glm::scale(modelview, scale);
	modelview = glm::rotate(modelview, degrees, rotateNums);
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawMesh(mesh,indexCount,GL_TRIANGLES);
}