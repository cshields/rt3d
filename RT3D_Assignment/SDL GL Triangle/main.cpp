
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include "rt3dObjLoader.h"
#include "label.h"
#include "Camera.h"
#include "md2model.h"
#include "LoadTexture.h"
#include "LoadObject.h"
#include "drawObject.h"
#include "Light.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include <vector>
#include <sstream>
#include <time.h>

using namespace std;
rt3d::materialStruct material0 = {
{0.8f, 0.8f, 0.8f, 1.0f}, // ambient
{0.8f, 0.8f, 0.8f, 1.0f}, // diffuse
{0.8f, 0.8f, 0.8f, 1.0f}, // specular
5.0f  // shininess
};

rt3d::materialStruct material1 = {
{0.8f, 0.8f, 0.8f, 1.0f}, // ambient
{0.8f, 0.8f, 0.8f, 1.0f}, // diffuse
{0.8f, 0.8f, 0.8f, 1.0f}, // specular
1.0f  // shininess
};


glm::vec3 character(0.0f, 1.1f, 0.0f);
glm::mat4 projection(1.0f);
glm::vec4 lightPos(0.0f, 3.0f, 0.0f, 1.0f);
stack<glm::mat4> mvStack;

GLuint indexCount;
GLuint squareVertCount;
GLuint coinIndexCount;
GLfloat distFromChar = -5.0f;
GLfloat r = 0.0f;
GLfloat coinRotate = 0.0f;
GLfloat scale = 1.0f;
int frameOrder = 1;
int counter = 4;
time_t timer;
time_t startTime;
time_t timeElapse;

GLuint shaderProgram;
GLuint skyboxProgram;
GLuint meshObjects[7];
GLuint textures [8];
GLuint skyboxT [5];
drawObject* skybox[4];
const int numOfbuildings = 9;
drawObject* buildings[numOfbuildings];
drawObject* externalBuildings[20];
drawObject* coins[4];
Light* source1;
Camera* camera;
drawObject* ground;
label* HUD;
LoadObject* cube;
LoadObject* square;
LoadObject* tarmac;
LoadObject* coin;
LoadObject* wall;
HSAMPLE *samples=NULL;

// md2 stuff
md2model goblin;
md2model yoshi;
int currentAnim = 0;
GLuint goblinVertCount = 0;
GLuint yoshiVertCount = 0;


SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);// 8 bit alpha buffering

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8); // Turn on x8 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void init(void) {
	skyboxProgram = rt3d::initShaders("textured.vert", "textured.frag");
	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,200.0f);
	camera = new Camera();
	samples = new HSAMPLE[2];
	HUD = new label;

	time(&startTime); // sets the start time for the timer

	if (!BASS_Init(-1,44100,0,0,NULL)) // initialize BASS
		cout << "Can't initialize device";

	// audio samples
	samples[0] = rt3d::loadSample("Dragon_Warrior_3_Flight_of_Destiny_OC_ReMix.mp3");
	samples[1] = rt3d::loadSample("shooting_star-Mike_Koenig-1132888100.wav");
	rt3d::playSound(samples[0]);

	source1 = new Light(0.2f, 0.2f, 0.2f);

	textures[0] = LoadTexture::loadBitmap("AsphaltCloseups0033_4_L.bmp", GL_REPEAT);
	textures[1] = LoadTexture::loadBitmap("BrickSmallBrown0293_12_S.bmp", GL_REPEAT);
	textures[2] = LoadTexture::loadBitmap("hobgoblin2.bmp", GL_REPEAT);
	textures[3] = LoadTexture::loadBitmap("buildingsHouseOld0122_L.bmp", GL_CLAMP_TO_EDGE);
	textures[4] = LoadTexture::loadBitmap("buildingsHouseOld0155_L.bmp", GL_CLAMP_TO_EDGE);
	textures[5] = LoadTexture::loadBitmap("buildingsNeoclassical0148_L.bmp", GL_CLAMP_TO_EDGE);
	textures[6] = LoadTexture::loadBitmap("yoshi.bmp", GL_REPEAT);
	textures[7] = LoadTexture::loadBitmap("20kr_2003.bmp", GL_REPEAT);

	skyboxT[0] = LoadTexture::loadBitmap("ofront7.bmp", GL_CLAMP_TO_EDGE);
	skyboxT[1] = LoadTexture::loadBitmap("oback7.bmp", GL_CLAMP_TO_EDGE);
	skyboxT[2] = LoadTexture::loadBitmap("oleft7.bmp", GL_CLAMP_TO_EDGE);
	skyboxT[3] = LoadTexture::loadBitmap("oright7.bmp", GL_CLAMP_TO_EDGE);
	skyboxT[4] = LoadTexture::loadBitmap("otop7.bmp", GL_CLAMP_TO_EDGE);

	// load meshes
	cube = new LoadObject("cube.obj");
	square = new LoadObject("square.obj");
	coin = new LoadObject("TyveKrone.obj");
	wall = new LoadObject("wallcube.obj");
	tarmac = new LoadObject("ground.obj");

	// set mesh index count
	squareVertCount = square->getindexCount();
	indexCount = cube->getindexCount();
	coinIndexCount = coin->getindexCount();

	meshObjects[0] = rt3d::createMesh(cube->getVertsData().size()/3, cube->getVertsData().data(), nullptr, cube->getNorms().data(), cube->getTex().data(), indexCount, cube->getIndices().data());
	meshObjects[1] = rt3d::createMesh(square->getVertsData().size()/3, square->getVertsData().data(), nullptr, square->getNorms().data(), square->getTex().data(), squareVertCount, square->getIndices().data());
	meshObjects[2] = goblin.ReadMD2Model("tris.MD2");
	meshObjects[3] = yoshi.ReadMD2Model("yoshi.MD2");
	meshObjects[4] = rt3d::createMesh(coin->getVertsData().size()/3, coin->getVertsData().data(), nullptr, coin->getNorms().data(), coin->getTex().data(), coinIndexCount, coin->getIndices().data());
	meshObjects[5] = rt3d::createMesh(wall->getVertsData().size()/3, wall->getVertsData().data(), nullptr, wall->getNorms().data(), wall->getTex().data(), indexCount, wall->getIndices().data());
	meshObjects[6] = rt3d::createMesh(tarmac->getVertsData().size()/3, tarmac->getVertsData().data(), nullptr, tarmac->getNorms().data(), tarmac->getTex().data(), squareVertCount, tarmac->getIndices().data());
	
	// set md2 index count
	goblinVertCount = goblin.getVertDataSize();
	yoshiVertCount = yoshi.getVertDataSize();

	// create the buildings and boundary walls
	buildings[0] = new drawObject(20.0f, 0.0f, glm::vec3(15.0,20.0f,15.0f), meshObjects[0], indexCount);
	buildings[1] = new drawObject(20.0f, 16.0f, glm::vec3(15.0,20.0f,15.0f), meshObjects[0], indexCount);
	buildings[2] = new drawObject(20.0f, 32.0f, glm::vec3(15.0,20.0f,15.0f), meshObjects[0], indexCount);
	buildings[3] = new drawObject(-20.0f, 0.0f, glm::vec3(15.0,20.0f,15.0f), meshObjects[0], indexCount);
	buildings[4] = new drawObject(0.0f, -35.0f, glm::vec3(40.0,15.0f,15.0f), meshObjects[0], indexCount);
	buildings[5] = new drawObject(0.0f, -40.0f, glm::vec3(80.0f,5.0f,1.0f), meshObjects[5], indexCount);
	buildings[6] = new drawObject(0.0f, 40.0f, glm::vec3(80.0f,5.0f,1.0f), meshObjects[5], indexCount);
	buildings[7] = new drawObject(40.0f, 0.0f, glm::vec3(1.0f,5.0f,80.0f), meshObjects[5], indexCount);
	buildings[8] = new drawObject(-40.0f, 0.0f, glm::vec3(1.0f,5.0f,80.0f), meshObjects[5], indexCount);
	
	// create  the buildings outside the level
	GLfloat z = -100;
	for (int i = 0;i < 5; i++){
		externalBuildings[i] = new drawObject(-100.0f, z, glm::vec3(15.0,30.0f,15.0f), meshObjects[0], indexCount);
		externalBuildings[i+5] = new drawObject(100.0f, z, glm::vec3(15.0,30.0f,15.0f), meshObjects[0], indexCount);
		externalBuildings[i+10] = new drawObject(z, -100.0f, glm::vec3(15.0,30.0f,15.0f), meshObjects[0], indexCount);
		externalBuildings[i+15] = new drawObject(z, 100.0f, glm::vec3(15.0,30.0f,15.0f), meshObjects[0], indexCount);
		z +=50;
	}

	coins[0] = new drawObject(0.0f, 20.0f, glm::vec3(0.2f,0.2f,0.2f), meshObjects[4], coinIndexCount);
	coins[1] = new drawObject(-35.0f, 0.0f, glm::vec3(0.2f,0.2f,0.2f), meshObjects[4], coinIndexCount);
	coins[2] = new drawObject(-30.0f, -30.0f, glm::vec3(0.2f,0.2f,0.2f), meshObjects[4], coinIndexCount);
	coins[3] = new drawObject(30.0f, -30.0f, glm::vec3(0.2f,0.2f,0.2f), meshObjects[4], coinIndexCount);

	skybox[0] = new drawObject(0.0f, -2.0f, glm::vec3(1.0,1.0f,1.0f), meshObjects[1], squareVertCount);
	skybox[1] = new drawObject(0.0f, 2.0f, glm::vec3(1.0,1.0f,1.0f), meshObjects[1], squareVertCount);
	skybox[2] = new drawObject(2.0f, 0.0f, glm::vec3(1.0,1.0f,1.0f), meshObjects[1], squareVertCount);
	skybox[3] = new drawObject(-2.0f, 0.0f, glm::vec3(1.0,1.0f,1.0f), meshObjects[1], squareVertCount);

	ground = new drawObject(0.0f, 0.0f, glm::vec3(50.0f, 0.1f, 50.0f), meshObjects[6], squareVertCount);

	rt3d::setLight(shaderProgram, source1->createLight());

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, 
	GL_ONE_MINUS_SRC_ALPHA);
}

void draw(SDL_Window * window) {
	// clear the screen
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set base position for scene
	glm::mat4 modelview(1.0f);
	mvStack.push(modelview);
	camera->SetAt(glm::vec3(character.x,character.y+1,character.z));
	camera->SetEye(glm::vec3(character.x,character.y+1,character.z),r,distFromChar);
	mvStack.top() = camera->getLookAt();

	// draw skybox
	glUseProgram(skyboxProgram);
	rt3d::setUniformMatrix4fv(skyboxProgram,"projection",glm::value_ptr(projection));

	glDepthMask(GL_FALSE);
	glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	mvStack.push(glm::mat4(mvRotOnlyMat3) );

	//front
	glBindTexture(GL_TEXTURE_2D, skyboxT[0]);
	skybox[0]->draw(skyboxProgram, mvStack.top());

	//back
	glBindTexture(GL_TEXTURE_2D, skyboxT[1]);
	skybox[1]->draw(skyboxProgram, mvStack.top(), glm::vec3(0.0f,1.0f,0.0f), 180.0f);

	//right
	glBindTexture(GL_TEXTURE_2D, skyboxT[2]);
	skybox[2]->draw(skyboxProgram, mvStack.top(), glm::vec3(0.0f,1.0f,0.0f), -90.0f);

	//left
	glBindTexture(GL_TEXTURE_2D, skyboxT[3]);
	skybox[3]->draw(skyboxProgram, mvStack.top(), glm::vec3(0.0f,1.0f,0.0f), 90.0f);
	mvStack.pop();

	glDepthMask(GL_TRUE);
	glUseProgram(shaderProgram);

	//reset projection matrix
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projection));

	// set light position
	glm::vec4 tmp = mvStack.top()*lightPos;
	rt3d::setLightPos(shaderProgram, glm::value_ptr(tmp));

	// draw a square for ground plane
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	rt3d::setMaterial(shaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -0.1f, 0.0f));
	ground->draw(shaderProgram, mvStack.top(), glm::vec3(1.0f, 0.0f, 0.0f), 90.0f);
	mvStack.pop();

	// Animate the md2 model, and update the mesh with new vertex data
	goblin.Animate(currentAnim,0.1, frameOrder);
	rt3d::updateMesh(meshObjects[2],RT3D_VERTEX,goblin.getAnimVerts(),goblin.getVertDataSize());

	// draw the hobgoblin
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, textures[2]);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), character);
	mvStack.top() = glm::rotate(mvStack.top(),r+90.0f,glm::vec3(0.0f,1.0f,0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(-1.0f,0.0f,0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.05, scale*0.05, scale*0.05));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(meshObjects[2], goblinVertCount/3, GL_TRIANGLES);
	mvStack.pop();

	// draw boundary walls
	glBindTexture(GL_TEXTURE_2D, textures[1]);
	buildings[5]->draw(shaderProgram, mvStack.top());
	buildings[6]->draw(shaderProgram, mvStack.top(), glm::vec3(0.0f,1.0f,0.0f), 180.0f);
	buildings[7]->draw(shaderProgram, mvStack.top(), glm::vec3(0.0f,1.0f,0.0f), 90.0f);
	buildings[8]->draw(shaderProgram, mvStack.top(), glm::vec3(0.0f,1.0f,0.0f), -90.0f);

	// draw Buildings
	glBindTexture(GL_TEXTURE_2D, textures[3]);
	buildings[0]->draw(shaderProgram, mvStack.top());
	buildings[1]->draw(shaderProgram, mvStack.top());
	
	glBindTexture(GL_TEXTURE_2D, textures[4]);
	buildings[2]->draw(shaderProgram, mvStack.top());
	buildings[3]->draw(shaderProgram, mvStack.top());

	glBindTexture(GL_TEXTURE_2D, textures[5]);
	buildings[4]->draw(shaderProgram, mvStack.top());

	// draw external buildings
	for (int i = 0;i < 20;i++)
		externalBuildings[i]->draw(shaderProgram, mvStack.top());

	glBindTexture(GL_TEXTURE_2D, textures[7]);
	// draw coins
	rt3d::setMaterial(shaderProgram, material0);
	for (int i = 0;i < 4;i++)
		if (coins[i] != NULL)
			coins[i]->draw(shaderProgram, mvStack.top(), 2.0f,glm::vec3(0.0f,1.0f,0.0f), coinRotate);

	// animate yoshi
	yoshi.Animate(2,0.1, 1);
	rt3d::updateMesh(meshObjects[3],RT3D_VERTEX,yoshi.getAnimVerts(),yoshi.getVertDataSize());

	//draw yoshi
	rt3d::setMaterial(shaderProgram, material1);
	mvStack.push(mvStack.top());
	glBindTexture(GL_TEXTURE_2D, textures[6]);
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-100.0f, 11.0f, 20.0f));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f,1.0f,0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(-1.0f,0.0f,0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.5, scale*0.5, scale*0.5));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawMesh(meshObjects[3], yoshiVertCount/3, GL_TRIANGLES);
	mvStack.pop();
	glCullFace(GL_BACK);

	glUseProgram(skyboxProgram);
	rt3d::setUniformMatrix4fv(skyboxProgram, "projection", glm::value_ptr(projection));

	// draw HUD
	glDepthMask(GL_FALSE);
	stringstream text;
	if (counter > 0)
		text << "Coins Left " << counter;
	else
		text << "You took " << timer << " seconds to collect all the Coins";
	HUD->textToTexture(text.str().c_str());
	HUD->draw(-1.0,1.0,skyboxProgram);
	
	glDepthMask(GL_TRUE);
	mvStack.pop();
    SDL_GL_SwapWindow(window); // swap buffers
}

void update (){
	Uint8 *keys = SDL_GetKeyboardState(NULL);

	if ( keys[SDL_SCANCODE_W] ){ 
		character = camera->moveForward(character,r,0.1f);
		currentAnim = 1;                                // sets the animation to running
		for(int i = 0; i < numOfbuildings;i++)
			if (buildings[i]->collision(character))
				character = camera->moveForward(character,r,-0.1f);
	}
	if ( keys[SDL_SCANCODE_S] ) {
		character = camera->moveForward(character,r,-0.1f);
		currentAnim = 13;			// sets the animation to CRwalk
		frameOrder = -1;			// sets the animation to play in reverse
		for(int i = 0; i < numOfbuildings;i++)
			if (buildings[i]->collision(character))
				character = camera->moveForward(character,r,0.1f);
	}
	if ( keys[SDL_SCANCODE_A] ) {
		character = camera->moveRight(character,r,-0.1f);
		for(int i = 0; i < numOfbuildings;i++)
			if (buildings[i]->collision(character))
				character = camera->moveRight(character,r,0.1f);
	}

	if ( keys[SDL_SCANCODE_D] ) {
		character = camera->moveRight(character,r,0.1f);
		for(int i = 0; i < numOfbuildings;i++)
			if (buildings[i]->collision(character))
				character = camera->moveRight(character,r,-0.1f);
	}
	if ( keys[SDL_SCANCODE_LEFT] ) r += 1.0f; // rotates the character left and right respectively
	if ( keys[SDL_SCANCODE_RIGHT] ) r -= 1.0f;

	coinRotate += 1.0;
	for(int i = 0; i < 4;i++)
		if (coins[i] != NULL)
			if (coins[i]->circleCollision(character)){
				rt3d::playSound(samples[1]);
				delete coins[i];
				coins[i] = NULL;
				counter--;
		}
	if (counter > 0){						// timer keeps counting till all the coins are collected
		time(&timeElapse);
		timer = difftime(timeElapse,startTime); // calculates time elapsed since the start of the launch	
	}

	for(int i = 0; i < numOfbuildings;i++)
		if (buildings[i]->collision(camera->getEye()))		// collision detection for the camera 
			distFromChar+=0.1f;

			if (distFromChar > -5.0)
				distFromChar-=0.05f;

	if ( keys[SDL_SCANCODE_ESCAPE] )
		exit(0);
}

int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		if (!SDL_PollEvent(&sdlEvent)){
			currentAnim = 0;			// sets the animation to idle when no keys are pressed
			frameOrder = 1;
		}
		update();
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}