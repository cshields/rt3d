#pragma once
#include <string>
#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glew.h>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "rt3d.h"

class label
{

private:
	GLuint texID;
	GLuint height;
	GLuint width;
	GLuint mesh;
	
	

public:
	label();
	void textToTexture(const char * str);
	void draw(float x, float y, const GLuint shader);
	void exitFatalError(char *message);
	~label(void);
};

