#pragma once
#include "rt3d.h"
class Light
{
private:
	GLfloat r;
	GLfloat g;
	GLfloat b;

public:
	Light(GLfloat r, GLfloat g, GLfloat b);
	~Light(void);
	rt3d::lightStruct createLight();
};

