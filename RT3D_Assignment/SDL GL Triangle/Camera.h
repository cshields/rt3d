#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

#define DEG_TO_RAD 0.017453293
class Camera
{
private:
	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;
public:
	Camera(void);
	~Camera(void);
	void SetAt(glm::vec3 focus){at = focus;}
	void SetEye(glm::vec3 cam, GLfloat angle, GLfloat d){eye = moveForward(cam,angle,d);}
	glm::vec3 getEye(){return eye;}
	glm::mat4 getLookAt(){return glm::lookAt(eye,at,up);}

	glm::vec3 moveForward(glm::vec3 cam, GLfloat angle, GLfloat d) {return glm::vec3(cam.x + d*std::sin(-angle*DEG_TO_RAD), cam.y, cam.z - d*std::cos(angle*DEG_TO_RAD));}

	glm::vec3 moveRight(glm::vec3 pos, GLfloat angle, GLfloat d) {return glm::vec3(pos.x + d*std::cos(angle*DEG_TO_RAD), pos.y, pos.z + d*std::sin(-angle*DEG_TO_RAD));}
};

