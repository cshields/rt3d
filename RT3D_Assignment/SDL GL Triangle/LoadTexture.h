#pragma once
#include "rt3d.h"

namespace LoadTexture{

	GLuint loadBitmap(const char *fname, GLint param);
}