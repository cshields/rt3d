#include "label.h"

label::label(){
}

void label::textToTexture(const char * str){
	TTF_Font* textFont;	// SDL type for True-Type font rendering

	if (TTF_Init()== -1)
		exitFatalError("TTF failed to initialise.");

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		exitFatalError("Failed to open font.");

	SDL_Surface *stringImage;
	SDL_Color colour = { 255, 0, 0 };
	stringImage = TTF_RenderText_Blended(textFont,str,colour);

	height = stringImage->h;
	width = stringImage->w;

	if (stringImage == NULL){
		exitFatalError("String surface not created.");
	}

	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;
	
	//GLuint texture;

	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, stringImage->w, stringImage->h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);

	SDL_FreeSurface(stringImage);
}

void label::draw(float x, float y, const GLuint shader){
	GLuint squareVertCount = 4;
	GLfloat vertices[] = {	x, y, 0.0f,
							x+0.004f*width, y, 0.0f,
							x+0.004f*width,y+0.004f*height, 0.0f,
							x, y+0.004f*height, 0.0f };

	GLfloat squareTexCoords[] = { 0.0f, 1.0f,
								1.0f, 1.0f,
								1.0f, 0.0f,
								0.0f, 0.0f };
	GLuint indexCount = 6;
	GLuint indices[] = { 0,1,2, 0,2,3 };

	mesh = rt3d::createMesh(squareVertCount, vertices, nullptr, vertices, squareTexCoords, indexCount, indices);

	glm::mat4 modelview(1.0);
	glBindTexture(GL_TEXTURE_2D, texID);
	modelview = glm::translate(modelview, glm::vec3(0.0f, 0.0f, -2.0f));
	rt3d::setUniformMatrix4fv(shader, "modelview", glm::value_ptr(modelview));
	rt3d::drawIndexedMesh(mesh,indexCount,GL_TRIANGLES);
	glDeleteTextures(1, &texID);
}


void label::exitFatalError(char *message)
{
    std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}

