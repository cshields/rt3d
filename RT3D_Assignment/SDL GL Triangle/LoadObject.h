#pragma once
#include "rt3dObjLoader.h"
#include <vector>

using namespace std;
class LoadObject
{
private:
	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	const char* object;
	GLuint indexCount;
public:
	LoadObject(const char *filename);
	~LoadObject(void);
	vector<GLfloat> getVertsData(){return verts;}
	vector<GLfloat> getNorms(){return norms;}
	vector<GLfloat> getTex(){return tex_coords;}
	vector<GLuint> getIndices(){return indices;}
	GLuint getindexCount(){return indexCount;}
};

