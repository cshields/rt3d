#include "LoadObject.h"


LoadObject::LoadObject(const char *filename)
{
	object = filename;
	rt3d::loadObj(object, verts, norms, tex_coords, indices);
	indexCount = indices.size();
}


LoadObject::~LoadObject(void)
{
}

