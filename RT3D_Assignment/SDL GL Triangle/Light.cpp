#include "Light.h"


Light::Light(GLfloat red, GLfloat green, GLfloat blue)
{
	r= red;
	g = green;
	b = blue;
}


Light::~Light(void)
{
}

rt3d::lightStruct Light::createLight()
{
	rt3d::lightStruct light0 = {
	{r, g, b, 1.0f}, // ambient
	{0.7f, 0.7f, 0.7f, 1.0f}, // diffuse
	{0.8f, 0.8f, 0.8f, 1.0f}, // specular
	};
	return light0;
}