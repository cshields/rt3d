#pragma once
#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
class building
{
private:
	glm::vec3 translate;
	glm::vec3 scale;
	GLfloat topCollision;
	GLfloat bottomCollision;
	GLfloat rightCollision;
	GLfloat leftCollision;
	GLuint mesh;
	GLuint indexCount; 
public:
	building(GLfloat transX, GLfloat transZ, glm::vec3 scaleNums, GLuint cubeMesh, GLuint cubeIndex);
	~building(void);
	void draw(const GLuint shader, glm::mat4 modelview);
	void draw(const GLuint shader, glm::mat4 modelview, glm::vec3 rotateNums, GLfloat degrees);
	bool collision(glm::vec3 characterPos);
};

